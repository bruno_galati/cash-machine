function askMoney(howMuch) {
      if(howMuch == null){
        return [];
    }
    howMuch = parseInt(howMuch);
    var drawer = [];
    var bills = [100.00,50.00, 20.00, 10.00];
    var currentValue = 0;
    var billsArray = [];
       
    //check for lowest bills
    bills = bills.sort(function(a,b){ return b-a; });
    var lowestNote = bills.map(Number).reduce(function(a, b) {
        return Math.min(a, b);
    });
        //if ordered money is multiple of the note itselft
    if ( ( howMuch % lowestNote ) != 0) {
      throw new Error('NoteUnavailableException');
    }else if(howMuch < 0){
        throw new Error('InvalidArgumentException');
    }
        //remove first bill from array
    var bill = bills.shift();
    while( ( howMuch > 0 ) && ( howMuch % lowestNote ) == 0 ){
        //check if current value is divisible by the bill in question
        currentValue = Math.floor( howMuch / bill );
        if ( ( howMuch % bill ) !=  0 && currentValue == 0) {
            bill = bills.shift();
            continue;
        }
               
        drawer.push({'note' : bill, 'times' : currentValue , 'howMuch' : howMuch});
        howMuch = howMuch - ( currentValue * bill);
        if (howMuch == 0) {
            break;
        }
    }
        //get the times that each note is going to be used and populate the array
    drawer.forEach(function(obj) {
        for(var i = 1; i <= obj.times; i++) {
            billsArray.push(obj.note);
        }
    });
    return billsArray;
}

console.log(askMoney(1250.00));